pragma solidity ^0.4.23;

// @title Auction builder.
contract AuctionFactory {
    
    struct Auction {
        string itemName;
        uint couponPriceInWei;
        uint duration;
        uint[] userCoupon;
        uint createdAt;
        uint winnerId;
    }

    address public owner;
    Auction[] public auctions;

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    constructor() public {
        owner = msg.sender;
    }

    // @dev Creates a new auction
    // @param _itemName Lot name
    // @param _couponPriceInWei Price in wei for 1 coupon
    // @param _duration Auction duration in seconds
    // @return auction id
    function create(string _itemName, uint _couponPriceInWei, uint _duration) external onlyOwner returns (uint) {
        require(_couponPriceInWei > 0);
        uint newLength = auctions.push(Auction({
            itemName: _itemName,
            couponPriceInWei: _couponPriceInWei,
            duration: _duration,
            userCoupon: new uint[](0),
            createdAt: now,
            winnerId: 0
        }));
        return newLength - 1;
    }

    // @dev Buys coupon by user and sends change back
    // @param _auctionId Auction id
    // @param _userId User/client id
    function buyCoupon(uint _auctionId, uint _userId) external payable {
        // check that auction exists
        require(auctions[_auctionId].couponPriceInWei > 0);
        // check that time has not passed
        require(now - auctions[_auctionId].duration < auctions[_auctionId].createdAt);
        uint couponCount = msg.value / auctions[_auctionId].couponPriceInWei;
        require(couponCount > 0);
        for(uint i = 0; i < couponCount; i++) {
            auctions[_auctionId].userCoupon.push(_userId);
        }
        // send change back
        uint change = msg.value - couponCount * auctions[_auctionId].couponPriceInWei;
        msg.sender.transfer(change);
    }

    // @dev Finishes auction
    // @param _auctionId Auction id(index)
    // @return winnerId
    function finish(uint _auctionId) external onlyOwner returns(uint) {
        // check that auction exists
        require(auctions[_auctionId].couponPriceInWei > 0);
        // check that auction has no winner
        require(auctions[_auctionId].winnerId == 0);
        // check that time passed
        require(now - auctions[_auctionId].duration > auctions[_auctionId].createdAt);
        // check that there is at least 1 coupon
        require(auctions[_auctionId].userCoupon.length > 0);
        // find random index and set winner
        uint winnerIndex = uint(blockhash(block.number - 1)) % auctions[_auctionId].userCoupon.length;
        auctions[_auctionId].winnerId = auctions[_auctionId].userCoupon[winnerIndex];
        return auctions[_auctionId].winnerId;
    }

}
