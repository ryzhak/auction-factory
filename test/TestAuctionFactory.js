const AuctionFactory = artifacts.require("AuctionFactory");

contract('TestAuctionFactory', function(accounts) {

    it('should set owner on contract deploy', function() {
        return AuctionFactory.deployed().then(function(instance) {
            return instance.owner.call().then(function(owner) {
                assert.equal(owner, accounts[0]);
            });
        });
    });

    it('should create an auction', function() {
        return AuctionFactory.deployed().then(function(instance) {
            return instance.create.call("ANY_NAME",10000000000000000,5).then(function(index) {
                assert.equal(index.toNumber(), 0);
            });
        });
    });

    it('should let user buy coupons', function() {
        return AuctionFactory.deployed().then(function(instance) {
            // buying 100 coupons for 1 ether
            return instance.buyCoupon.call(0,1,{from: accounts[1], value: 1000000000000000000}).then(function() {
                return instance.auctions.call().then(function(auctions){
                    assert.equal(auctions[0].userCoupon.length, 100);
                });
            });
        });
    });

    it('should finish an auction', function() {
        return AuctionFactory.deployed().then(function(instance) {
            return instance.finish.call(0).then(function() {
                return instance.auctions.call().then(function(auctions){
                    assert.equal(auctions[0].winnerId, 1);
                });
            });
        });
    });

});
